'use strict';

var BPromise = require('bluebird');

module.exports = function() {
  BPromise.promisifyAll(require('mongoose'));
  BPromise.promisifyAll(require('request'));
};