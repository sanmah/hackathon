var User = require('../models/user');
var uuid = require('node-uuid');

 function registrationHelper(newUser) {
	 newUser.id = newUser.email;
	 newUser.score = Math.round(Math.random() * 100);
	 var user = new User(newUser);
	 return User.findOneAsync({ id: newUser.id })
		.then(function(doc) {
			if (doc) {
				throw new Error('User already exists');
			} else {
				return user.saveAsync()
					.then(function(done) {
						console.log('User successfully registered.');
						return loginHelper(user.id, user.password)
							.then(function(userDetails) {
								return userDetails;
							});
					});
			}
		})
		.error(function(e) {
			console.log(e);
			throw e;
		});
}

function loginHelper (username, password) {
	return User.findOne({ id: username }).lean().execAsync()
		.then(function (user) {
			if (!user) {
				return null;
			} else {
				var borrower = true;
				if (user.hasOwnProperty('lender')) {
					borrower = false;
				}
				if (password !== user.password) {
					return null;
				} else {
					var accessToken;
					if (user.accessToken) {
						return { borrower: borrower, accessToken: user.accessToken };
					} else {
						accessToken = uuid.v4();
						return User.updateAsync({ id: user.id }, { '$set': { accessToken: accessToken } })
							.then(function(done) {
								return { borrower: borrower, accessToken: accessToken };
							});
					}
				}
			}
		})
		.error(function(err) {
			console.log(err);
			throw err;
		});
}

function getUserFromAccessToken(accessToken) {
	return User.findOne({ accessToken: accessToken }).lean().execAsync()
		.then(function(user) {
			return user;
		});
}

function logoutHelper(user) {
	return User.updateAsync({ _id: user._id }, { "$unset": { accessToken: "" } })
		.then(function() { return true; });
}

module.exports = {
	loginHelper: loginHelper,
	registrationHelper: registrationHelper,
	getUserFromAccessToken: getUserFromAccessToken,
	logoutHelper: logoutHelper
};