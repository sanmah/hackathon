
require('../globals');
var Transaction = require('../models/transaction');

Transaction.destroy({}, function(error, response) {
    if(error) {
        Log.e(error);
        return;
    }

    process.exit();
});