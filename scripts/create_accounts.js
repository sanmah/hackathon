
require('../globals');
var async = require('async');
var accounts = require('../accounts.json');
var User = require('../models/user');
var Transaction = require('../models/transaction');

//Log.i("Removing all users and transactions");
// originally we were clearing out users and transactions, but simply specifying
// helps
//User.destroy({}, function(error, response) {
//    if(error) {
//        Log.e(error);
//        return;
//    }
//
//    Transaction.destroy({}, function(error, response) {
//        if(error) {
//            Log.e(error);
//            return;
//        }
//    });
//});
var fns = [];
Log.i("Creating preset accounts - update accounts.json to add contacts");
_.each(accounts, function(account) {
    fns.push(function(done) {
        User.find({
            "uid": account.uid
        }, function(error, user) {
            if(error) {
                Log.e(error);
                return;
            }

            if(user && user[0]) {
                user[0].set(account);
                user[0].save(null, done);
            } else {
                var user = new User(account);
                user.save(null, done)
            }
        })
    });
});

async.parallel(fns, function(errors, results) {
    if(errors) {
        Log.e("Errors while creating users: ");
        Log.e(errors);
        return;
    }

    Log.i("Done - ", results.length, "Results Completed");

    _.each(results, function(result) {
        Log.i("User updated: ", result[0].get("name"));
        //Log.i("Access Token: ", result[0].encryptedToken());
    });

    process.exit();
});