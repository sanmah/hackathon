Using NPM with visa proxy:

$ npm config set proxy http://internet.visa.com:80
$ npm config set httpsproxy https://internet.visa.com:443


Running tests:

mocha test
mocha test/accounts

Adding tests
Add to index.js if its meant for everyone to run every time
Please add tests

To push to heroku, do
git init
And add to .git/config the following lines
[remote "heroku"]
        url = https://git.heroku.com/flow-sandbox.git
        fetch = +refs/heads/*:refs/remotes/heroku/*


*** THIS DOESN'T WORK -- USE LOCALHOST FOR DEV ***
In some cases, you may need to add these for using the proxy. For example, if you see this error message:

BackboneMongo: unable to create connection. Error: { [MongoError: getaddrinfo ENOTFOUND ds047911.mongolab.com]
  name: 'MongoError',
  message: 'getaddrinfo ENOTFOUND ds047911.mongolab.com' }
Connection failed. Error: getaddrinfo ENOTFOUND ds047911.mongolab.com

export HTTP_PROXY=http://internet.visa.com:80
export HTTPS_PROXY=https://internet.visa.com:443
*** /THIS DOESN'T WORK ***