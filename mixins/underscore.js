var _ = require("underscore");

_.mixin({
    capitalize: function(string) {
        return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
    },
    isEmail: function(email) {
        var format = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
        return (email.match(format));
    },
    cleanPhone: function(string) {
        var numbers = [];
        var match;
        var newString = string;
        while(match = newString.match(/(\d+)/)) {
            numbers.push(match[1]);
            newString = newString.replace(match[1]);
        }
        return numbers.join('').toString();
    }
})