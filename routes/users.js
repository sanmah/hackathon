var express = require('express');
var router = express.Router();
var userService = require('../services/userService');

router.get('', function(req, res) {
  userService.getUsersByType(req.user, req.query.type)
    .then(function(users) {
      res.status(200).json(users);
    })
    .catch(function(err) {
      res.status(400).send({'message': 'Invalid input.'});
    })
  
});

router.post('/:id/mark', function(req, res) {
  userService.markUserInterest(req.user, req.params.id)
    .then(function(result) {
        res.status(200).json(result);
    })
    .catch(function(err) {
      res.status(500).send();
    });
});

router.get('/me', function(req, res) {
  userService.getCurrentUser(req.user)
    .then(function(user) {
      res.status(200).json(user);
    })
    .catch(function(err) {
      res.status(500).send();
    });
});

router.post('/:id/accept', function(req, res) {
  userService.acceptLoan(req.user, req.params.id, req.body.emiDetails)
    .then(function(result) {
      res.status(200).json(result);
    })
    .catch(function(err) {
      res.status(500).send();
    });
});

module.exports = router;
