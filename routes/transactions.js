var Backbone = require('backbone');

var express = require('express');
var router = express.Router();
var async = require('async');

var Constants = require('../constants');

var User = require('../models/user');
var Transaction = require('../models/transaction');

router.get('/', function(req, res, next) {
    var mongoQuery;
    var limit = req.query.limit || 20;
    var offset = req.query.offset || 0;

    if(req.user && req.user.id) {
        if(req.query.query) {
            var query = req.query.query.split(',');

            mongoQuery = {
                "$or": [{
                    "$and": [{
                        "user_id": req.user.id
                    }, {
                        "receiver_uid": {
                            "$in": query
                        }
                    }]
                }, {
                    "$and": [{
                        "sender_uid": {
                            "$in": query
                        }
                    }, {
                        "receiver_id": req.user.id
                    }]
                }]
            };
        } else {
            mongoQuery = {
                "user_id": req.user.id.toString()
            };
        }

        mongoQuery["$limit"] = limit;
        mongoQuery["$offset"] = offset;
        mongoQuery["$sort"] = req.query.sort || "-created";

        Transaction.find(mongoQuery, function(error, transactions) {
            if (error) {
                Log.e(error.message);
                Log.e(error.stack);
                return res.status(500).json({
                    "error": "Internal server error"
                });
            }

            // it appears that the backbone-orm brings back a format of attributes.attributes for a
            // collection found using their relationships
            // which is likely a bug
            var mapped = _.map(transactions, function (transaction) {
                return transaction.private();
            });

            return res.json({
                transactions: mapped
            });
        });
    } else {
        res.status(401).json({ "error": "Unauthenticated request" });
    }
});

router.get('/:id', function(req, res, next) {
    if(req.user && req.user.id) {
        Transaction.findOne({
            "id": req.params.id
        }, function (error, transaction) {
            if(error) {
                Log.e(error);
                Log.e(error.stack);
                return res.status(500).json({
                    "error": "Internal server error"
                });
            }
            var sender = transaction.get('user');
            if (req.user.id === sender.id) {
                return res.json(transaction.private());
            } else {
                return res.status(402).json({
                    "error": "Unauthorized fetch"
                });
            }
        })
    } else {
        return res.status(401).json({
            "error": "Authorization required"
        });
    }
});

// /transactions: create and process
router.post('/', function(req, res, next) {
    if(req.user && req.user.id) {
        var body = req.body;
        var sender = req.user;

        var receiver_uids = body.receiver_uids || body.receiver_uid;

        if(!receiver_uids) {
            return res.status(412).json({
                "error": "Receiver required (receiver_uid: <email_or_phone> } )"
            });
        }

        // turn into array if its a string
        if(!_.isArray(receiver_uids)) {
            receiver_uids = [receiver_uids];
        }

        var amount = parseInt(req.body.amount) || 0;

        if(amount === 0) {
            return res.status(412).json({
                "error": "Amount required"
            });
        }

        var currency = req.body.currency;

        if(!currency) {
            return res.status(412).json({
                "error": "Currency required"
            });
        }


        req.user.fetch({
            success: function() {
                var fns = [];
                // async
                _.each(receiver_uids, function(receiver_uid) {
                    fns.push(function(done) {
                        User.findOne({
                            uid: receiver_uid
                        }, function (error, receiver) {
                            if (error) {
                                Log.e(error);
                                Log.e(error.stack);
                                res.status(500).json({
                                    "error": "Internal server error"
                                });
                                done(error);
                                return;
                            }

                            var transaction = new Transaction();
                            transaction.set('amount', amount);
                            transaction.set('currency', currency);
                            transaction.set('status', Constants.Transactions.CREATED);
                            transaction.set('user', req.user);
                            transaction.set('user_id', req.user.id);
                            transaction.set('sender_id', req.user.id);
                            transaction.set('sender_uid', req.user.get('uid'));
                            transaction.set('sender_name', req.user.getName());
                            transaction.set('type', req.body.type);

                            if (receiver && receiver.id) {
                                transaction.set('receiver', receiver);
                                transaction.set('receiver_id', receiver.id);
                                transaction.set('receiver_uid', receiver.get('uid'));
                                transaction.set('receiver_name', receiver.get('name'));
                            } else {
                                transaction.set('receiver_uid', receiver_uid);
                            }

                            transaction.set('created', new Date());

                            if (_.isString(req.body.memo)) {
                                transaction.set('memo', req.body.memo);
                            }

                             .callOCT(function (error, data) {
                                transaction.save(null, {
                                    success: function () {
                                        transaction.sendPushNotification();
                                        done(null, transaction);
                                    },
                                    error: function (transaction, error) {
                                        res.status(500).json({
                                            "error": "Internal server error"
                                        });
                                        done(error);
                                    }
                                });
                            });

                        });
                    });
                });

                async.parallel(fns, function(error, results) {
                    var ids = "";
                    if(results) {
                        ids = _.map(results, function(result) {
                            if(result) {
                                return result.id;
                            }
                        });
                    }
                    if(error) {
                        Log.e("Error creating transaction");
                        Log.e(error);
                        Log.e(error.stack);
                        if(ids.length) {
                            res.status(201).json({
                                "success": "Partial success",
                                "ids": ids,
                                "error": error
                            });
                            return;
                        } else {
                            res.status(500).json({
                                "error": error
                            });
                            return;
                        }
                    } else {
                        res.status(201).json({
                            "success": "Transaction" + (results.length !== 1 ? "s": "") + " Created",
                            "ids": ids
                        });
                        return;
                    }
                });
            },
            error: function(user, error) {
                Log.e(error);
                Log.e(error.stack);
                return res.status(500).json({
                    "error": "Internal server error"
                });
            }
        });
    } else {
        return res.status(401).json({ "error": "Unauthenticated request" });
    }
});

router.delete('/', function(req, res, next) {
    if(req.user && req.user.id) {
        Transaction.destroy({
           "user_id": req.user.id
        }, function(error, response) {
            if(error) {
                Log.e(error);
                return res.status(500).json({ "error": "Internal server error" });
            }
            return res.status(200).json({ "success": "Deleted Transactions" });
        });
    } else {
        return res.status(401).json({ "error": "Unauthenticated request" });
    }
});

module.exports = router;
