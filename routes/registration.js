var utility = require('../helpers/utility');

module.exports.login = function(req, res) {
	var username = req.body.username;
	var password = req.body.password;
	return utility.loginHelper(username, password)
		.then(function(userDetails) {
			if (userDetails) {
				res.status(200).json(userDetails);
			} else {
				res.status(400).send({ message: 'Invalid username or password' });
			}
		})
		.error(function(err) {
			res.status(500).send();
		});
};

module.exports.register = function(req, res) {
	var newUser = req.body;
	return utility.registrationHelper(newUser)
		.then(function(userDetails) {
			if (userDetails) {
				res.status(200).json(userDetails);
			} else {
				res.status(400).send({ message: 'Invalid username or password' });
			}
		})
		.error(function(err) {
			res.status(400).send({message: 'Registration failed'});
		});
};

module.exports.logout = function(req, res) {
    return utility.logoutHelper(req.user)
        .then(function() {
            console.log("Logged out successfully");
            res.status(200).send();
        })
        .catch(function(err) {
            res.status(500).send();
        });
};

