var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var accounting = require('accounting');
require('backbone.touch');

var TransactionCollection = require('../collections/transaction');

var template = require('../templates/user.jade');
var editTemplate = require('../templates/user_edit.jade');
var transactionTemplate = require('../templates/transaction.jade');

var vex = require('vex-js/js/vex.combined.min');
vex.defaultOptions.className = 'vex-theme-os';

module.exports = Backbone.View.extend({
    tagName: "tr",
    events: {
        "click td a.transactions": "onTransactions",
        "click": "onEdit",
        "click .checkin": "checkIn"
    },
    initialize: function(options) {
        this.model = options.model;
    },
    getEmails: function() {
        var data = this.model.get('data') || {};
        return _.map(data.emails, function(email) {
            return email.address;
        }).join(', ');
    },
    getNumbers: function() {
        var data = this.model.get('data') || {};
        return _.map(data.numbers, function(number) {
            return number.number;
        }).join(', ')
    },
    render: function() {
        var data = this.model.get('data') || {};
        var emails = this.getEmails();
        var numbers = this.getNumbers();

        this.$el.html(template({
            user: this.model.toJSON(),
            emails: emails,
            numbers: numbers
        }));

        return this;
    },
    onEdit: function(options) {
        options = options || {};
        var emails = this.getEmails();
        var numbers = this.getNumbers();
        var that = this;

        var isNew = this.model.isNew();

        vex.open({
            "message": (isNew ? "Create": "Edit") + " User",
            input: editTemplate({
                user: this.model.toJSON(),
                emails: emails,
                numbers: numbers
            }),
            callback: function(data) {
                if(data) {
                    var emailsArr = data.emails.split(',');
                    var numbersArr = data.numbers.split(',');

                    var emails = [];

                    if(data.emails != "") {
                        emails =_.map(emailsArr, function (email) {
                            return {
                                "address": $.trim(email),
                                "type": "home"
                            };
                        });
                    }

                    var numbers = [];

                    if(data.numbers != "") {
                        numbers = _.map(numbersArr, function (number) {
                            return {
                                "number": $.trim(number),
                                "type": "mobile"
                            };
                        });
                    }

                    delete data.emails;
                    delete data.numbers;

                    this.model.set(data);
                    this.model.set('data', {
                        emails: emails,
                        numbers: numbers
                    });

                    this.model.save(null, {
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": this.model.get('access_token')
                        },
                        success: function(model, response, body) {
                            if(response.success === "User created") {
                                // post to verify to get an access token and save
                                $.post('/accounts/verify', {
                                    "uid": model.get('uid'),
                                    "verify_token": "123-123"
                                }, function(response) {
                                    // re set data and save
                                    this.model.set(data);
                                    this.model.set('data', {
                                        emails: emails,
                                        numbers: numbers
                                    });

                                    this.model.set('access_token', response.access_token);

                                    this.model.save(null, {
                                        headers: {
                                            "Content-Type": "application/json",
                                            "Authorization": this.model.get('access_token')
                                        },
                                        success: function() {
                                            that.render();
                                            if(options.onCreate) {
                                                options.onCreate();
                                            }
                                        },
                                        error: function() {
                                            alert("Error saving user");
                                        }
                                    });
                                }.bind(this));
                            } else {
                                that.render();
                            }
                        }.bind(this),
                        error: function() {
                            alert("Error saving user");
                        }
                    });
                } else {

                }
            }.bind(this)
        })
    },
    onTransactions: function(e) {
        e.stopPropagation();

        var transactions = new TransactionCollection();
        transactions.fetch({
            headers: {
                "Content-Type": "application/json",
                "Authorization": this.model.get("access_token")
            },
            success: function() {

                var html = [];

                html.push("<table>");
                html.push("<thead><th>Sender</th><th>Receiver</th><th>Amount</th></thead>");
                transactions.each(function(transaction) {
                    html.push(transactionTemplate({
                        transaction: transaction.toJSON(),
                        amount: accounting.formatMoney(transaction.get("amount"))
                    }));
                });
                html.push("</table>");
                vex.open({
                    "message": "Transactions",
                    input: html.join(""),
                    buttons: [
                        _.extend({}, vex.buttons.NO, { text: 'OK' }),
                        _.extend({}, vex.buttons.YES, { text: 'Clear Transactions' })
                    ],
                    callback: function (data) {
                        if(data) {
                            $.ajax({
                                method: "DELETE",
                                url: "/transactions",
                                headers: {
                                    "Authorization": this.model.get("access_token")
                                },
                                success: function() {

                                },
                                error: function() {
                                    alert("Error clearing transactions");
                                }
                            })

                        } else {

                        }
                    }.bind(this)
                });

            }.bind(this),
            error: function() {
                alert("Error fetching transactions");
            }
        });
    },

    checkIn: function(e) {
        e.stopPropagation();
        e.preventDefault();

        this.model.set('checked_in', true);
        this.render();

        this.model.save(null, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": this.model.get('access_token')
            },
            success: function (model, response, body) {
            }.bind(this),
            error: function() {
                this.model.unset("checked_in");
                this.render();
                alert("Error checking in user");
            }
        });
    }
});