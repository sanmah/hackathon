var Backbone = require('backbone');
var template = require('../templates/home.jade');
var User = require('../models/user');
var UserListItem = require('../views/user_list_item');

var AppView = Backbone.View.extend({
    events: {
        'click .add-user': 'addUser'
    },
    render: function(){
        this.$el.html(template());
        return this;
    },
    renderUsers: function(users) {
        this.user_views = this.users_views || [];
        var $user_list = this.$el.find('.users-list tbody');
        // reset
        $user_list.html("");
        users.each(function(user) {
            var view = new UserListItem({
                model: user
            });
            this.user_views.push(view);
            $user_list.append(view.render().$el);
        }.bind(this));
    },
    addUser: function() {
        var user = new User();
        // simple - use the existing list item with its onEdit
        // without attaching to the DOM
        var view = new UserListItem({
            model: user
        });
        view.onEdit({
            onCreate: function() {
                var $user_list = this.$el.find('.users-list tbody');
                $user_list.append(view.render().$el);
            }.bind(this)
        });
    }
});

module.exports = AppView;