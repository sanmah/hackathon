var Backbone = require('backbone');
var Transaction = require('../models/transaction');

module.exports = Backbone.Collection.extend({
    model: Transaction,
    url: '/transactions/',
    parse: function(data) {
        if(data && data.transactions) {
            return data.transactions;
        }
        return [];
    }

});