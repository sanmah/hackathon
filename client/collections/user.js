var Backbone = require('backbone');
var User = require('../models/user');

module.exports = Backbone.Collection.extend({
    model: User,
    url: '/accounts/lookup',
    parse: function(data) {
        if(data && data.users) {
            return data.users;
        }
        return [];
    }

});