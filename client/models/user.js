var _ = require('underscore');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
    url: '/accounts/',
    parse: function(data) {
        // so that saving POSTS to /accounts/ and PUTS to /accounts/
        // as without ever actually getting an id from the server
        if(data.uid) {
            data.id = data.uid;
        }
        return data;
    }
});