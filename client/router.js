var $ = require('jquery');
var Backbone = require('backbone');
var UserCollection = require('./collections/user');

module.exports = Backbone.Router.extend({
    routes: {
        'home': 'homeView'
    },

    homeView: function() {
        var AppView = require('./views/app_view');
        var appView = new AppView();
        $('main').html(appView.render().$el);

        var users = new UserCollection();

        users.fetch({
            headers: {
              "Authorization": "letmein"
            },
            data: {
                limit: 1000
            },
            success: function() {
                appView.renderUsers(users);
            },
            error: function(model, xhr, response) {

            }
        })
    }
});