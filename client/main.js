/**
 * Created by dbadley on 6/30/15.
 */
var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

$(document).ready(function() {
    var AppRouter = require('./router');
    new AppRouter();
    Backbone.history.start({ pushState: true });
});