require('./globals');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var User = require('./models/user');

var registration = require('./routes/registration');
var users = require('./routes/users');

var utility = require('./helpers/utility');


var app = express();
require('./helpers/promisify')();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/login', registration.login);
app.post('/register', registration.register);

app.use(function(req, res, next) {
  var accessToken = req.headers.authorization;
  if(accessToken) {
    utility.getUserFromAccessToken(accessToken)
      .then(function(user) {
        if (user && user.id) {
          req.user = user;
        } else {
          res.status(403).send();
        }
        next();    
      });
    }
});

app.use('/users', users);
app.post('/logout', registration.logout);

mongoose.connect("mongodb://localhost:27017/hackathondb", function (err, db, next) {
    if (!err) {
        console.log("mongoDB instance connected successfully.");
    }
    else {
        console.log("DB Connection failed");
        //  logger.error(err.stack);
        next(err);
    }
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// will print stacktrace
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: err
  });
});

module.exports = app;
