var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var User =  new Schema({
    id: { type: String, required: true, unique: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    phone: { type: String, required: true },
    email: { type: String, required: true },
    address: { type: String },
    password: { type: String, required: true },
    card: {
        pan: {type: String, required: true },
        cvv: { type: String },
        expiry: { type: String },
        name: { type: String }
    },
    borrower: {
        amount: { type: Number }
    },
    lender: {
        amount: { type: Number },
        rate: { type: Number },
        term: { type: Number }
    },
    interestedInYou: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    interestedIn: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    score: { type: Number },
    accessToken: { type: String }
});

User.index({ 'id': 1 });

module.exports = mongoose.model('User', User);