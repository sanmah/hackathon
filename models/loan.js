var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Loan = new Schema({
	lender: { type: Schema.Types.ObjectId, ref: 'User' },
	borrower: { type: Schema.Types.ObjectId, ref: 'User' },
	amount: { type: Number, required: true },
	rate: { type: Number, required: true },
	term: { type: Number, required: true },
	emi: {
			amount: { type: Number },
			date: { type: Number },
			status: { type: String }
	}
});

Loan.index({ 'id': 1 });

module.exports = mongoose.model('Loan', Loan);