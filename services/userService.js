var _ = require('lodash');
var User = require('../models/user');
var mongoose = require('mongoose');
var Loan  = require('../models/loan');
//var BPromise = require('bluebird');
var transactionService = require('./transactionService');

function getCurrentUser(currentUser) {
	return User.findOne({ id: currentUser.id }, { accessToken: 0, password: 0 }).populate('interestedIn interestedInYou').lean().execAsync()
    .then(function(user) {
		var usersInterestedIn = _.map(user.interestedIn, function(user) { return user.email; });
		user.mutuallyAccepted = _.map(user.interestedInYou, function(user) { if(_.contains(usersInterestedIn, user.email)) { return user; } });	
		return user;
    })
    .error(function(err) {
      console.log(err);
      throw err;
    });
}

function getUsersByType(currentUser, userType) {
	var query = {};
	var sort = {};
	if (userType === 'lender') {
		query = { 'lender.amount': { '$gte': currentUser.borrower.amount } };
		sort = { 'lender.rate': 1 };
	} else if (userType === 'borrower') {
		query = { 'borrower.amount': { '$lte': currentUser.lender.amount } };
		sort = { 'score': -1 };
	} else {
		throw new Error('Please select a type of user');
	}
	return User.find(query, { accessToken: 0, password: 0 }).sort(sort).lean().execAsync()
	.then(function(users) {
	  var interestedIn = _.map(currentUser.interestedIn, function(objectId) { return objectId.toHexString(); });
	  var updatedUsers = _.map(users, function(user) {
	    if (_.contains(interestedIn, user._id.toHexString())) {
	      user.expressedInterestByYou = true;
	    }
	    return user;  
	  });
	  return updatedUsers;  
	})
	.error(function(err) {
	  console.log(err);
	  throw err;
	});
};

function markUserInterest(currentUser, targetUserId) {
	var interestedIn = new mongoose.Types.ObjectId(targetUserId);
    var existingRefs = _.map(currentUser.interestedIn, function(user) { return user.toHexString(); });
    if (!_.contains(existingRefs, targetUserId)) {
        return User.updateAsync({ id: currentUser.id }, { '$push': { 'interestedIn': interestedIn }})
            .then(function() { return User.updateAsync({ _id: targetUserId }, { '$push': { 'interestedInYou': currentUser._id } }); })
            .then(function() { return User.findOne({ id: currentUser.id }).lean().execAsync(); })
            .then(function(user) {
                return user;
            })
            .error(function(err) {
                console.log(err);
                throw err;
            });
    } else {
        return currentUser;
    }
};

function acceptLoan(borrower, targetUserId, emiDetails) {
	return User.findOne({ _id: targetUserId }).lean().execAsync()
		.then(function(lender) {
			var loan = new Loan();
			loan.lender = lender._id;
			loan.borrower = borrower._id;
			loan.amount = borrower.borrower.amount;
			loan.rate = lender.lender.rate;
			loan.term = emiDetails.term;
			loan.emi =  {
				amount: emiDetails.amount,
				date: 1
			};
			return loan.saveAsync()
				.then(function() { return User.updateAsync({ id: borrower.id }, { '$pull': { 'interestedIn': lender._id, 'interestedInYou': lender._id }}); })
				.then(function() { return User.updateAsync({ _id: targetUserId }, { '$pull': { 'interestedIn': borrower._id, 'interestedInYou': borrower._id } }); })
				.then(function() { return transactionService.pullFunds(lender, loan.amount); })
				.then(function() { return transactionService.pushFunds(borrower, loan.amount); })
				.then(function() { return loan })
				.error(function(err) {
					console.log(err);
					throw err;
				});		
		});
}


module.exports = {
	getCurrentUser: getCurrentUser,
	getUsersByType:  getUsersByType,
	markUserInterest: markUserInterest,
	acceptLoan: acceptLoan
};