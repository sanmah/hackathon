var https = require('https');
var crypto = require('crypto');
var HOST = 'qaperf.api.visa.com';
var apikey = "092V0HTRN9BQJ3P1Y3AR21e-cAZM4wn_yFbtXASCN-GjIXEDA";
var sharedSecret = "9py5vvgHaQ4GD1ICquO@gjJ@N7eqwqo-80EEHETx";
var request = require('request');

exports.pullFunds = function(sender, amount, callback) {
	var senderPrimaryAccountNumber = 4957030098899026;
	var payload = JSON.stringify({
		  "SystemsTraceAuditNumber": 300259,
		  "RetrievalReferenceNumber": "407509300259",
		  "DateAndTimeLocalTransaction": "2021-10-26T21:32:52",
		  "AcquiringBin": 409999,
		  "AcquirerCountryCode": "101",
		  "SenderPrimaryAccountNumber": senderPrimaryAccountNumber,
		  "SenderCardExpiryDate": "2017-03",
		  "SenderCurrencyCode": "USD",
		  "Amount": "112.00",
		  "Surcharge": "2.00",
		  "Cavv": "0000010926000071934977253000000000000000",
		  "ForeignExchangeFeeTransaction": "10.00",
		  "BusinessApplicationID": "AA",
		  "MerchantCategoryCode": 6012,
		  "CardAcceptor": {
		    "Name": "Malissa V",
		    "TerminalId": "365539",
		    "IdCode": "VMT200911026070",
		    "Address": {
		      "State": "CA",
		      "County": "081",
		      "Country": "USA",
		      "ZipCode": "94404"
		    }
		  },
		  "MagneticStripeData": {
		    "track1Data": "1010101010101010101010101010"
		  },
		  "PointOfServiceData": {
		    "PanEntryMode": "90",
		    "PosConditionCode": "0",
		    "MotoECIIndicator": "0"
		  },
		  "PointOfServiceCapability": {
		    "PosTerminalType": "4",
		    "PosTerminalEntryCapability": "2"
		  },
		  "FeeProgramIndicator": "123"
	});	
	
	var apiPath = "ft/AccountFundingTransactions";
    var path = "/pm/ft/AccountFundingTransactions?apikey=" + apikey;
    var acceptHeader = 'application/vnd.visa.FundsTransfer.v1+json';
    var timestamp = Math.floor(new Date().getTime() / 1000);
    var beforeHash = sharedSecret + timestamp + apiPath + "apikey="+ apikey + payload;
    var hash = crypto.createHash('sha256').update(beforeHash).digest('hex');
    hash = hash.toLowerCase();
    var postHeaders = {
        "Content-Type" : 'application/json',
        "x-pay-token" : 'x:'+timestamp+':'+hash ,
        'accept': acceptHeader
    };
    
    var reqOptions = {
        url: "https://" + HOST + path,
        headers: postHeaders,
        body: payload
    };

    return request.postAsync(reqOptions)
        .spread(function(response, body) {
            if (response.statusCode !== 200) {
                console.log(body);
                return true;
                //throw new Error('Visa direct service call failed.');
            } else {
                return true;
            }   
        })
        .error(function(err) {
            console.log(err);
            throw err;
        });
};

exports.pushFunds = function(receiver, amount, callback) {
	var senderAccountNumnber = "4957030098899042";
    var recipientPrimaryAccountNumber = "4957030098899026";
    var payload = JSON.stringify({
        "SystemsTraceAuditNumber": 350420,
        "RetrievalReferenceNumber": "401010350420",
        "DateAndTimeLocalTransaction": "2021-10-26T21:32:52",
        "AcquiringBin": 409999,
        "AcquirerCountryCode": "101",
        "SenderReference": "",
        "SenderAccountNumber": senderAccountNumnber,
        "SenderCountryCode": "USA",
        "TransactionCurrency": "USD",
        "SenderName": "lendr",
        "SenderAddress": "44 Market St.",
        "SenderCity": "San Francisco",
        "SenderStateCode": "CA",
        "RecipientCardPrimaryAccountNumber": recipientPrimaryAccountNumber,
        "Amount": amount,
        "BusinessApplicationID": "AA",
        "MerchantCategoryCode": 6012,
        "TransactionIdentifier": 234234322342343,
        "SourceOfFunds": "03",
        "CardAcceptor": {
	        "Name": "John Smith",
	        "TerminalId": "13655392",
	        "IdCode": "VMT200911026070",
	        "Address": {
	            "State": "CA",
	            "County": "081",
	            "Country": "USA",
	            "ZipCode": "94105"
	        }
    	},
    	"FeeProgramIndicator": "123"
	});

    var apiPath = "ft/OriginalCreditTransactions";
    var path = "/pm/ft/OriginalCreditTransactions?apikey=" + apikey;
    var acceptHeader = 'application/vnd.visa.FundsTransfer.v1+json';
    var timestamp = Math.floor(new Date().getTime() / 1000);
    var beforeHash = sharedSecret + timestamp + apiPath + "apikey="+ apikey + payload;
    var hash = crypto.createHash('sha256').update(beforeHash).digest('hex');
    hash = hash.toLowerCase();
    var postHeaders = {
        "Content-Type" : 'application/json',
        "x-pay-token" : 'x:'+timestamp+':'+hash ,
        'accept': acceptHeader
    };
    
    var reqOptions = {
        url: "https://" + HOST + path,
        headers: postHeaders,
        body: payload
    };
    
    return request.postAsync(reqOptions)
        .spread(function(response, body) {
            if (response.statusCode !== 200) {
                //throw new Error('Visa direct service call failed.');
                console.log(body);
                return true;
            } else {
                return true;
            }   
        })
        .error(function(err) {
            console.log(err);
            throw err;
        });	
};
